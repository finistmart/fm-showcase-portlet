package com.finistmart.showcase.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown=true)
public class SettingsResponse {

    private String serviceUrl;
    private List<DataMappingEntry> dataMapping;
    private List<Map<String, Object>> productData;
    private String template;
    private Map<String, String> l11n;

    public SettingsResponse() {
    }

    public SettingsResponse(String serviceUrl, List<DataMappingEntry> dataMapping, List<Map<String, Object>> productData, String template) {
        this.serviceUrl = serviceUrl;
        this.dataMapping = dataMapping;
        this.productData = productData;
        this.template = template;
    }

    public SettingsResponse(String serviceUrl, List<DataMappingEntry> dataMapping, List<Map<String, Object>> productData, String template, Map<String, String> l11n) {
        this.serviceUrl = serviceUrl;
        this.dataMapping = dataMapping;
        this.productData = productData;
        this.template = template;
        this.l11n = l11n;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public List<DataMappingEntry> getDataMapping() {
        return dataMapping;
    }

    public void setDataMapping(List<DataMappingEntry> dataMapping) {
        this.dataMapping = dataMapping;
    }

    public List<Map<String, Object>> getProductData() {
        return productData;
    }

    public void setProductData(List<Map<String, Object>> productData) {
        this.productData = productData;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public Map<String, String> getL11n() {
        return l11n;
    }

    public void setL11n(Map<String, String> l11n) {
        this.l11n = l11n;
    }
}
