package com.finistmart.showcase.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class DataMappingEntry {
    private String sl;
    private String valExp;
    private String valType;
    private String valTmpl;

    public DataMappingEntry() {
    }

    public String getSl() {
        return sl;
    }

    public void setSl(String sl) {
        this.sl = sl;
    }

    public String getValExp() {
        return valExp;
    }

    public void setValExp(String valExp) {
        this.valExp = valExp;
    }

    public String getValType() {
        return valType;
    }

    public void setValType(String valType) {
        this.valType = valType;
    }

    public String getValTmpl() {
        return valTmpl;
    }

    public void setValTmpl(String valTmpl) {
        this.valTmpl = valTmpl;
    }
}
