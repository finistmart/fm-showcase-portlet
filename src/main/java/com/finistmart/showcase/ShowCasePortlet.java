package com.finistmart.showcase;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.finistmart.showcase.dto.DataMappingEntry;
import com.finistmart.showcase.dto.SettingsResponse;
import com.finistmart.showcase.i18n.Utf8ResourceBundleControl;
import org.apache.commons.io.IOUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URLDecoder;
import java.util.*;


import javax.portlet.*;
import javax.servlet.ServletRequest;
import javax.servlet.ServletRequestWrapper;


public class ShowCasePortlet extends GenericPortlet {

	ObjectMapper jsonMapper = new ObjectMapper();


	public void serveResource(ResourceRequest request, ResourceResponse response) throws PortletException, IOException {
		if (request.getResourceID().equals("list")) {
			response.setCharacterEncoding("application/json");
			PrintWriter out = response.getWriter();
			Map<String, String> data = new HashMap<>();
			data.put("price", "100");
			
			//out.write(jsonMapper.writeValueAsString(settings));
			out.flush();
			out.close();
		} else if (request.getResourceID().equals("getSettings")) {

			response.setContentType("application/json");
			PortletPreferences portletPreferences = request.getPreferences();
			String serviceUrl = portletPreferences.getValue("showCaseServiceUrl", "http://localhost");
			String dataSetting = portletPreferences.getValue("productData", "[]");
			String dataMappingSetting = portletPreferences.getValue("dataMapping", "[]");
			String template = portletPreferences.getValue("showCaseTemplate", "<div>empty item template</div>");

			List<DataMappingEntry> dataMapping = new ArrayList<>();
			List<Map<String, Object>> data = new ArrayList<>();
			try {
				dataMapping = jsonMapper.readValue(dataMappingSetting, new TypeReference<List<DataMappingEntry>>(){});
				data = jsonMapper.readValue(dataSetting, new TypeReference<List<Map<String, Object>>>(){});
			} catch (Exception e) {
				e.printStackTrace();
			}

			Map<String, String> translations = getTranslationsMap(request, response);

			PrintWriter out = response.getWriter();
			SettingsResponse settingsResponse = new SettingsResponse(serviceUrl, dataMapping, data, template, translations);

			out.write(jsonMapper.writeValueAsString(settingsResponse));
			out.flush();
			out.close();
		}
	}


	private Map<String, String> getTranslationsMap(ResourceRequest request, ResourceResponse response) {
		Map<String, String> translations = new HashMap<>();
		// :TODO  to correct lang code
		ResourceBundle.Control utf8Control =
				new Utf8ResourceBundleControl();
		ResourceBundle resources = ResourceBundle.getBundle("locale/ShowCasePortlet_" + response.getLocale().getLanguage(), request.getLocale(), utf8Control);
		Enumeration<String> key = resources.getKeys();

		while (key.hasMoreElements()) {
			String param = (String) key.nextElement();
			translations.put("tr." + param, resources.getString(param));
		}
		return translations;
	}


	public String extParam(String value) throws UnsupportedEncodingException {
		return URLDecoder.decode(new String(Base64.getDecoder().decode(value)), "UTF-8");
	}

	public void processAction(ActionRequest request, ActionResponse response) throws PortletException,
			java.io.IOException {
		// :TODO liferay dependant code must be wrapped
		ServletRequestWrapper wrapper = (ServletRequestWrapper) request.getAttribute("com.liferay.portal.kernel.servlet.PortletServletRequest");
		ServletRequest servletRequest = ((ServletRequestWrapper) (wrapper.getRequest())).getRequest();
		try {
			PortletPreferences portletPreferences = request.getPreferences();
			portletPreferences.setValue("showCaseServiceUrl", extParam(servletRequest.getParameter("serviceUrl")));
			portletPreferences.setValue("dataMapping", extParam(servletRequest.getParameter("dataMapping")));
			portletPreferences.setValue("productData", extParam(servletRequest.getParameter("productData")));
			portletPreferences.setValue("showCaseTemplate", extParam(servletRequest.getParameter("template")));
			portletPreferences.store();
		} catch (ReadOnlyException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ValidatorException e) {
			e.printStackTrace();
		}
	}

	private String prefOrResDefault(RenderRequest request, String name, String defFileName) {
		PortletPreferences portletPreferences = request.getPreferences();

		String data = portletPreferences.getValue(name, null);
		ClassLoader classLoader = getClass().getClassLoader();
		if (data == null) {
			InputStream is = classLoader.getResourceAsStream(defFileName);
			StringWriter writer = new StringWriter();
			try {
				IOUtils.copy(is, writer, "UTF-8");
			} catch (IOException e) {
				e.printStackTrace();
			}
			data = writer.toString();

		}
		return data;
	}

	protected void doView(RenderRequest renderRequest, RenderResponse renderResponse)
		throws PortletException, IOException {

		renderRequest.setAttribute("productData", prefOrResDefault(renderRequest, "productData", "sample-data.json"));

		renderRequest.setAttribute("dataMapping", prefOrResDefault(renderRequest, "dataMapping", "sample-data-mapping.json"));

		renderRequest.setAttribute("template", prefOrResDefault(renderRequest, "template", "sample-template.html"));

		String viewPath = "/WEB-INF/views/showCaseView.jsp";
		PortletRequestDispatcher portletRequestDispatcher =
				getPortletContext().getRequestDispatcher(viewPath);
		portletRequestDispatcher.include(renderRequest, renderResponse);
	}
}
