


export class ShowCaseConfigElement extends HTMLElement {

    //renderer: Renderer;
    //eventBus: EventBus;

    constructor() {
        super();
        console.log('showcase config element ctor');
    }

    connectedCallback() {
        this.renderTemplate();
        this.bindActions();

    }

    renderTemplate() {
        this.template = document.getElementById(this.ns + 'template-showcase-config');
        this.appendChild(this.template.content);
    }

    open() {
        this.configDialog.showModal();
    }

    bindActions() {

        this.closeButton = this.querySelector('#' + this.ns + 'closeDialogBtn');
        this.configDialog = this.querySelector('#' + this.ns + 'configDialog');


        this.closeButton.addEventListener('click', (event) => {
            this.configDialog.close();
        });

        this.saveButton = this.querySelector('#' + this.ns + 'saveDialogBtn');
        this.saveButton.addEventListener('click', (event) => {
            this.saveParams();
        });
    }

    saveParams() {
        let serviceUrlInput = this.querySelector('input[name=serviceUrl]');

        let dataEdit = this.querySelector('textarea[name=productData]');
        let dataMappingEdit = this.querySelector('textarea[name=dataMapping]');
        let templateEdit = this.querySelector('textarea[name=template]');

        var xhr = new XMLHttpRequest();
        xhr.open("POST", this.saveUrl, true);
        xhr.setRequestHeader("Content-Type", 'application/x-www-form-urlencoded');
        xhr.send('serviceUrl=' + encodeURIComponent(btoa(encodeURIComponent(serviceUrlInput.value))) + '&productData=' + encodeURIComponent(btoa(encodeURIComponent(dataEdit.value))) +
            '&dataMapping=' + encodeURIComponent(btoa(encodeURIComponent(dataMappingEdit.value))) + '&template=' + encodeURIComponent(btoa(encodeURIComponent(templateEdit.value))));
        xhr.onreadystatechange = (res) => {
            if(res.readyState == XMLHttpRequest.DONE) {
                if (res.status == 200) {
                    this.onSaved(res);
                }
            }
        }
    }

    onSaved(res) {
        console.log('onSaved', res);
    }

    get ns() {
        return this.getAttribute('ns') || '';
    }

    get saveUrl() {
        return this.getAttribute('saveUrl') || '';
    }
}
