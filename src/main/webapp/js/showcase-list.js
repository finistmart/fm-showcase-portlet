
import { jsonPath } from './jsonpath-0.8.0.js'


export class ShowCaseListElement extends HTMLElement {

    //renderer: Renderer;
    //eventBus: EventBus;


    // template;
    // itemTemplateSl;
    // itemTemplate;
    // dialogSl;
    // previewDialog;

    // settings = {};

    // cancelBtn;
    // addToCartBtn;
    // btnBuy;

    // imageUrls = [];
    // maxImageIndex: number;
    // curImageIndex: number;

    // btnPrev;
    // btnNext;

    constructor() {
        super();
        console.log('showcase list element ctor');
        this.itemTemplateSl = this.ns + 'templateShowcaseItem';
        this.dialogSl = this.ns + 'previewDialog';
        this.templateSl = this.ns + 'templateShowcaseList';
        this.showCaseContainerSl = '.fm-showcase-container';
        this.previewImageSl = '.fm-showcase-preview-image';
        this.showCasePreviewPropSl = '.fm-showcase-preview-';

        this.btnPrevSl = '.btn-prev';
        this.btnNextSl = '.btn-next';
        this.btnCancelSl = '.btn-cancel';
        this.btnAddToCartSl = '.btn-addtocart';
        this.btnBuySl = '.btn-buy';
    }

    connectedCallback() {
        this.loadSettings().then((settings) => {
            this.settings = settings;
            this.renderTemplate(settings);
        });
    }

    loadSettings() {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.open("GET", this.settingsUrl);
            xhr.setRequestHeader("Content-Type", 'application/json');
            xhr.onreadystatechange = (event) => {
                if(event.target.readyState === XMLHttpRequest.DONE) {
                    if (event.target.status === 200) {
                        let settings = JSON.parse(event.target.response);
                        resolve(settings);
                    } else {
                        reject(event);
                    }
                }
            };

            xhr.send();
        });
    }

    translate(settings, el) {
        el = el ? el : this;
        let trElements = el.querySelectorAll('[class^=tr]');
        if (trElements !== null) {
            trElements.forEach((el, index) => {
                console.log('tr el', el, index);
                for (let className of el.classList) {
                    if (className.startsWith('tr-')) {
                        let normedClassName = className.replace(/-/g, '.');
                        let tr = this.settings.l11n[normedClassName];
                        if (tr) {
                            el.textContent = tr;
                        }
                    }
                }
            });
        }
    }

    addToCart(event, data) {
        document.dispatchEvent(new CustomEvent('cart:additem', { bubbles: true, detail: { item: data }}));
    }

    buy(event, data) {
        this.addToCart(event, data);
        document.dispatchEvent(new CustomEvent('cart:show'));
        document.dispatchEvent(new CustomEvent('cart-list:itemsselected'));
    }

    prevImage(data) {
        console.log('prevImage', data);
        this.curImageIndex = (this.curImageIndex > 0) ? --this.curImageIndex : 0;

        this.renderSlideImage();
        this.updateNavButtons();
    }

    updateNavButtons() {
        if (this.curImageIndex === 0) {
            this.btnPrev.classList.add('fm-showcase-inactive');
        } else {
            this.btnPrev.classList.remove('fm-showcase-inactive');
        }
        if (this.curImageIndex < this.maxImageIndex) {
            this.btnNext.classList.remove('fm-showcase-inactive');
        } else {
            this.btnNext.classList.add('fm-showcase-inactive');
        }
    }

    nextImage(data) {
        console.log('nextImage', data);

        if (this.curImageIndex < this.maxImageIndex) {
            this.curImageIndex++;
        }

        this.renderSlideImage();
        this.updateNavButtons();
    }

    renderImage(imageUrl) {
        if (imageUrl) {
            let image = new Image();
            image.onload = () => {
                let imageEl = this.previewDialog.querySelector(this.previewImageSl);
                imageEl.innerHTML = '';
                imageEl.appendChild(image);
            };
            image.src = imageUrl;
        }
    }

    renderSlideImage() {
        let imageUrl = this.imageUrls[this.curImageIndex];

        this.renderImage(imageUrl);
    }

    renderNavButtons(data) {
        let imageUrls =  data.images && data.images.indexOf(',') ? data.images.split(',') : [];

        if (imageUrls.length > 0) {
            this.imageUrls = [data.image, ...imageUrls];

            this.maxImageIndex = (this.imageUrls.length > 0) ? this.imageUrls.length - 1 : 0;

            this.btnPrev = this.previewDialog.querySelector(this.btnPrevSl);
            this.btnNext = this.previewDialog.querySelector(this.btnNextSl);

            if (this.btnNext !== null && this.btnPrev !== null) {
                if (this.maxImageIndex > 0) {
                    this.btnPrev.addEventListener('click', (event) => {
                        this.prevImage(data);
                    });
                    this.btnPrev.classList.add('fm-showcase-inactive');
                    this.curImageIndex = 0;
                    this.btnNext.addEventListener('click', (event) => {
                        this.nextImage(data);
                    });
                } else {
                    this.btnPrev.setAttribute('display', 'none');
                    this.btnNext.setAttribute('display', 'none');
                }
            }
        }
    }

    renderActionButtons(data) {
        this.cancelBtn = this.previewDialog.querySelector(this.btnCancelSl);
        if (this.cancelBtn !== null) {
            this.cancelBtn.addEventListener('click', (event) => {
                this.previewDialog.close();
            });
        }
        this.addToCartBtn = this.previewDialog.querySelector(this.btnAddToCartSl);
        if (this.addToCartBtn !== null) {
            this.addToCartBtn.addEventListener('click', (event) => {
                this.addToCart(event, data);
                this.previewDialog.close();
            });
        }
        this.btnBuy = this.previewDialog.querySelector(this.btnBuySl);
        if (this.btnBuy !== null) {
            this.btnBuy.addEventListener('click', (event) => {
                this.buy(event, data);
                this.previewDialog.close();
            });
        }
    }

    showItem(event) {
        this.itemTemplate = document.importNode(document.getElementById(this.itemTemplateSl), true);
        this.previewDialog = document.getElementById(this.dialogSl);
        this.previewDialog.innerHTML = '';
        this.previewDialog.appendChild(this.itemTemplate.content);

        console.log('target', event.target);
        if (event.target) {
            let data = event.target.parentElement.dataset;

            // :TODO do from settings template in more universal manner
            for (let propName of ['title', 'price', 'currency']) {
                let el = this.previewDialog.querySelector(this.showCasePreviewPropSl + propName);
                if (el !== null) {
                    el.textContent = data[propName];
                }
            }

            this.renderImage(data.image);
            this.renderActionButtons(data);
            this.renderNavButtons(data);
            this.previewDialog.showModal();
        }
        this.translate(this.settings, this.previewDialog);
    }

    renderTemplate(settings) {
        this.template = document.getElementById(this.templateSl);

        this.appendChild(document.importNode(this.template.content, true));

        let listContainer = this.querySelector(this.showCaseContainerSl);
        if (listContainer !== null) {
            if (settings !== undefined) {
                let template = settings.template;
                let productData = settings.productData;
                for (let product of productData) {
                    let itemContainer = document.createElement('div');
                    itemContainer.classList.add('fm-showcase-item');
                    itemContainer.insertAdjacentHTML('beforeend', template);
                    let mappings = settings.dataMapping || [];
                    for (let mapping of mappings) {
                        let el = itemContainer.querySelector(mapping.sl);
                        if (mapping.valType && mapping.valType === 'style' && mapping.valTmpl) {
                            el.style = mapping.valTmpl.replace('%%', jsonPath(product, mapping.valExp));
                        } else {
                            if (el !== null) {
                                el.textContent = jsonPath(product, mapping.valExp);
                            }
                        }
                    }
                    for (let property in product) {
                        itemContainer.setAttribute('data-' + property, product[property]);
                    }
                    itemContainer.onclick = this.showItem.bind(this);
                    listContainer.appendChild(itemContainer);
                }
            }
        }
    }


    get ns() {
        return this.getAttribute('ns') || '';
    }

    get settingsUrl() {
        return this.getAttribute('settingsUrl') || '';
    }
}
