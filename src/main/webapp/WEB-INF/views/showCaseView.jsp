<%@ page import="javax.portlet.PortletPreferences" %>
<?xml version="1.0" encoding="UTF-8"?>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<portlet:defineObjects />

<%
	String ctxPath = request.getContextPath();
%>

<portlet:actionURL var="saveSettingsURL" name="saveSettings"></portlet:actionURL>
<portlet:resourceURL var="getSettingsURL" id="getSettings"></portlet:resourceURL>

<script nomodule>
	if (!window.babelPluginForLoop) {
	    document.write('<script src=<%= ctxPath %>/vendor/babel-browser-build.js><\/script>');
		document.write('<script src=<%= ctxPath %>/vendor/browser-es-module-loader.js><\/script>');
		document.write('<script src=<%= ctxPath %>/vendor/custom-elements-es5-adapter.js><\/script>');
		document.write('<script src=<%= ctxPath %>/vendor/webcomponents-bundle.js><\/script>');
	}
</script>

<style>
	.fm-btn-configure {
		background-image: url('<%= ctxPath %>/images/settings_black_192x192.png');
		background-size: contain;
		width: 50px;
		height: 50px;
		float: right;
		margin-top: -50px;
	}
	.fm-showcase-container {
		display: flex;
		flex-wrap: wrap;
	}
	.fm-showcase-btn-cancel {
		float: left;
	}
	.fm-showcase-btn {
		margin-top: 0.5em;
		margin-right: 0.5em;
	}
	.fm-showcase-btn-addtocart, .fm-showcase-btn-buy {
		float: right;
	}
	.fm-showcase-item {
		width: 200px;
		height: 200px;
		background: #000;
		margin: 5px;
		color: #fff;
	}
	.fm-showcase-image {
		height: 100%;
	}
	.fm-showcase-title {
		position: relative;
		bottom: 190px;
		left: 10px;
		z-index: 200;
		height: 1em;
	}
	.fm-showcase-price {
		position: relative;
		bottom: 60px;
		left: 140px;
		z-index: 200;
	}
	.fm-showcase-preview-pricebox {
		position: relative;
		z-index: 200;
		clear: both;
		float: right;
		right: 100px;
		display: flex;
		flex-direction: row;
		align-items: center;
	}
	.fm-showcase-preview-currency {
		padding-left: 10px;
	}

	.fm-form-field label {
		min-width: 120px;
	}
	.fm-form-text {
		width: 550px;
		height:200px;
	}
	.fm-showcase-btn-prev {
		padding-right: 20px;
	}
	.fm-showcase-btn-next {
		padding-left: 20px;
	}
	.fm-showcase-inactive {
		opacity: 0.2;
	}
	.fm-showcase-preview-title {
		font-size: 2em;
	}
	.fm-showcase-preview-price {
		float: right;
		font-size: 1.2em;
	}
	.fm-showcase-actions {
		clear: both;
	}
	.fm-showcase-preview {
		display: flex;
		flex-direction: row;
		padding: 20px;
	}
	.fm-showcase-btn-nav {
		padding-top: 35%;
		font-size: 4em;
		line-height: 0;
		cursor: pointer;
	}
	dialog {
		top: 50%;
	}
	dialog:not([open]) {
		display: none !important;
	}
</style>
<%
	String serviceUrl = (String) renderRequest.getAttribute("serviceUrl");
	String productData = (String) renderRequest.getAttribute("productData");
	String dataMapping = (String) renderRequest.getAttribute("dataMapping");
	String template = (String) renderRequest.getAttribute("template");
%>
<template id="${renderResponse.getNamespace()}template-showcase-config">
	<dialog id="${renderResponse.getNamespace()}configDialog" class="my-modal">
		<h3>Settings</h3>
		<div class="fm-form-field">
			<label>Service Url</label>
			<input type="text" name="serviceUrl" value="<%= serviceUrl %>" />
		</div>
		<div class="fm-form-field">
			<label>Or Data</label>
			<textarea name="productData" class="fm-form-text"><%= productData %></textarea>
		</div>
		<div class="fm-form-field">
			<label>Data Mapping</label>
			<textarea name="dataMapping" class="fm-form-text"><%= dataMapping %></textarea>
		</div>
		<div class="fm-form-field">
			<label>Data Template</label>
			<textarea name="template" class="fm-form-text"><%= template %></textarea>
		</div>
		<div class="actions">
			<button id="${renderResponse.getNamespace()}saveDialogBtn" class="small ok-modal-btn">Save</button>
			<button id="${renderResponse.getNamespace()}closeDialogBtn" class="small close-modal-btn">Close</button>
		</div>
	</dialog>
</template>
<template id="${renderResponse.getNamespace()}templateShowcaseList">
	<div class="fm-showcase-container">

	</div>
</template>
<template id="${renderResponse.getNamespace()}templateShowcaseItem">
	<h2 class="fm-showcase-preview-title"></h2>
	<div class="fm-showcase-preview">
		<span class="fm-showcase-btn-prev fm-showcase-btn-nav btn-prev">&larr;</span>
		<div class="fm-showcase-preview-image"></div>
		<span class="fm-showcase-btn-next fm-showcase-btn-nav btn-next">&rarr;</span>
	</div>
	<div class="fm-showcase-preview-pricebox">
		<div class="fm-showcase-preview-price"></div>
		<div class="fm-showcase-preview-currency"></div>
	</div>
	<div class="fm-showcase-actions">
		<button class="tr-showcase-btn-cancel fm-showcase-btn fm-showcase-btn-cancel btn-cancel">Cancel</button>

		<button class="tr-showcase-btn-buy fm-showcase-btn fm-showcase-btn-buy btn-buy">Buy</button>
		<button class="tr-showcase-btn-addtocart fm-showcase-btn fm-showcase-btn-addtocart btn-addtocart">Add To Cart</button>
	</div>
</template>
<dialog id="${renderResponse.getNamespace()}previewDialog" class="my-modal">

</dialog>

<% if (renderRequest.isUserInRole("administrator")) { %>
	<showcase-config id="${renderResponse.getNamespace()}configDialog"
					 ns="${renderResponse.getNamespace()}"
					 saveUrl="${saveSettingsURL.toString()}">
	</showcase-config>
	<button class="fm-btn-configure" id="${renderResponse.getNamespace()}configDialogOpenBtn"></button>
<% } %>

<showcase-list ns="${renderResponse.getNamespace()}" settingsUrl="${getSettingsURL.toString()}"></showcase-list>

<script type="module">

	import { Renderer } from '<%= ctxPath %>/node_modules/complets/renderer.js';
	import { Registry } from '<%= ctxPath %>/node_modules/complets/registry.js';
	import { EventBus } from '<%= ctxPath %>/node_modules/complets/eventbus.js';

	import { ShowCaseListElement } from '<%= ctxPath %>/js/showcase-list.js';
    import { ShowCaseConfigElement } from '<%= ctxPath %>/js/showcase-config.js';

	window.fmRegistry = window.fmRegistry || new Registry();

	fmRegistry.wire({
		EventBus,
		Renderer,
		ShowCaseListElement: { def: ShowCaseListElement, deps: {
				'renderer': Renderer,
				'eventBus': EventBus,
			}, is: 'showcase-list'},
		ShowCaseConfigElement: { def: ShowCaseConfigElement, deps: {
				'renderer': Renderer,
				'eventBus': EventBus,
			}, is: 'showcase-config'},
	});

    let openButton = document.querySelector('#${renderResponse.getNamespace()}configDialogOpenBtn');
    let configDialog = document.querySelector('#${renderResponse.getNamespace()}configDialog');
    if (configDialog != null) {
		openButton.addEventListener('click', (event) => {
			configDialog.open();
		});
	}

</script>


